package io.gitlab.hsedjame.mongo.cascade.operations.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Project MYALDOC_PORJECT
 * @Author Henri Joel SEDJAME
 * @Date 30/01/2019
 * @Class purposes : .......
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface MongoCascade {

    public enum CascadeType {ALL, SAVE, DELETE}

    CascadeType[] type() default {};
}
