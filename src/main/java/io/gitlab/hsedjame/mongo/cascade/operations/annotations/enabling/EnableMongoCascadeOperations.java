package io.gitlab.hsedjame.mongo.cascade.operations.annotations.enabling;

import io.gitlab.hsedjame.mongo.cascade.operations.configuration.CascadingMongoEventListener;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Project MYALDOC_PORJECT
 * @Author Henri Joel SEDJAME
 * @Date 24/01/2019
 * @Class purposes : .......
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(value= {
        CascadingMongoEventListener.class
})
public @interface EnableMongoCascadeOperations {
}
