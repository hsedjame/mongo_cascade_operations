package io.gitlab.hsedjame.mongo.cascade.operations.callbacks;

import io.gitlab.hsedjame.mongo.cascade.operations.annotations.MongoCascade;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.function.Consumer;

import static io.gitlab.hsedjame.mongo.cascade.operations.utils.MongoCascadeUtils.contains;
import static io.gitlab.hsedjame.mongo.cascade.operations.utils.MongoCascadeUtils.execute;

@Slf4j
public class CascadeFieldCallBack implements ReflectionUtils.FieldCallback {

    public enum Operations {
        SAVE,
        DELETE
    }

    private Consumer<Object> consumer;
    private Object source;
    private Operations operation;

    public CascadeFieldCallBack(Object source, Operations operation, Consumer<Object> consumer) {
        this.source = source;
        this.consumer = consumer;
        this.operation = operation;
    }

    @Override
    public void doWith(Field field) {

        if (field.isAnnotationPresent(MongoCascade.class)) {

            final MongoCascade annotation = field.getAnnotation(MongoCascade.class);

            final MongoCascade.CascadeType[] types = annotation.type();

            final boolean forSave = contains(types, MongoCascade.CascadeType.SAVE, MongoCascade.CascadeType.ALL) && operation.equals(Operations.SAVE);

            final boolean forDelete = contains(types, MongoCascade.CascadeType.DELETE, MongoCascade.CascadeType.ALL) && operation.equals(Operations.DELETE);

            if (forSave || forDelete) {
                execute(field, source, consumer);
            }

        }
    }

}
