package io.gitlab.hsedjame.mongo.cascade.operations.configuration;

import io.gitlab.hsedjame.mongo.cascade.operations.callbacks.CascadeFieldCallBack;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.util.List;

import static io.gitlab.hsedjame.mongo.cascade.operations.utils.MongoCascadeUtils.isMongoDocument;

@Slf4j
@Component
public class CascadingMongoEventListener extends AbstractMongoEventListener {

    //********************************************************************************************************************
    // ATTRIBUTS
    //********************************************************************************************************************

    private final MongoOperations mongoOperations;
    public CascadingMongoEventListener(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    //********************************************************************************************************************
    // METHODES
    //********************************************************************************************************************

    @Override
    public void onBeforeConvert(BeforeConvertEvent event) {
        Object source = event.getSource();
        ReflectionUtils.doWithFields(source.getClass(), new CascadeFieldCallBack(source, CascadeFieldCallBack.Operations.SAVE, this::doSave));
    }

    @Override
    public void onBeforeDelete(BeforeDeleteEvent event) {
        Class type = event.getType();
        final Object id = event.getDocument().get("_id");
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        final List<?> objects = mongoOperations.find(query, type);
        if(!objects.isEmpty()){
            final Object source = objects.get(0);
            ReflectionUtils.doWithFields(type, new CascadeFieldCallBack(source, CascadeFieldCallBack.Operations.DELETE, this::doDelete));
        }
    }

    //********************************************************************************************************************
    // METHODES PRIVATE
    //********************************************************************************************************************

    private void doSave(Object field) {
        if (isMongoDocument(field)) mongoOperations.save(field);
    }

    private void doDelete(Object field) {
        if (isMongoDocument(field)) mongoOperations.remove(field);
    }

}
