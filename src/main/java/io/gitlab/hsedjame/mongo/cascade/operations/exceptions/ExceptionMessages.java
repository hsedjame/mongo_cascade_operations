package io.gitlab.hsedjame.mongo.cascade.operations.exceptions;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @Project MYALDOC_PORJECT
 * @Author Henri Joel SEDJAME
 * @Date 24/01/2019
 * @Class purposes : .......
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionMessages {
    public static final String CASCADE_ERROR = "Une erreur est survenue lors de la manipulation par cascade de l'attribut {0} du l'objet {1}";

    public static final String  TRYING_SAVE_NO_DOCUMENT_OBJECT = "L'objet que vous essayez de sauvegarder n'est pas pas annoté @Document ou ne possède pas d'attribut annoté @Id";
}
