package io.gitlab.hsedjame.mongo.cascade.operations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication()
public class MongoCascadeOperationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongoCascadeOperationsApplication.class, args);
    }

}
