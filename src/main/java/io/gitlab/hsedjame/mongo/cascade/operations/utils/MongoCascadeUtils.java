package io.gitlab.hsedjame.mongo.cascade.operations.utils;

import io.gitlab.hsedjame.mongo.cascade.operations.exceptions.ExceptionMessages;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.mapping.MappingException;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * @Project MYALDOC_PORJECT
 * @Author Henri Joel SEDJAME
 * @Date 30/01/2019
 * @Class purposes : .......
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MongoCascadeUtils {

    public static void execute(Field field,Object source, Consumer<Object> consumer)  {
        ReflectionUtils.makeAccessible(field);

        final String name = field.getName();
        final String letter = String.valueOf(name.charAt(0));
        try {
            final Method method = source.getClass().getMethod("get" + name.replaceFirst(letter, letter.toUpperCase()));
            final Object attribut = method.invoke(source);

            if (attribut instanceof Collection){
                for (Object fieldValue :  ((Collection) attribut)){
                    consumer.accept(fieldValue);
                }
            } else {
                Object fieldValue = field.get(source);
                consumer.accept(fieldValue);
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            log.error(ExceptionMessages.CASCADE_ERROR);
        }
    }

    public static boolean contains(Object[] objects, Object object){
        if(objects.length == 0) return false;
        for (Object object1 : objects) {
            if (object.equals(object1)) return true;
        }
        return false;
    }

    public static boolean contains(Object[] objects, Object... objs) {
        if (objects.length == 0) return false;
        for (Object obj : objs) {
            if (contains(objects, obj)) return true;
        }
        return false;
    }

    public static boolean isMongoDocument(Object fieldValue) {
        if (Objects.isNull(fieldValue)) return false;
        if (!fieldValue.getClass().isAnnotationPresent(Document.class)
                || (Arrays.stream(fieldValue.getClass().getDeclaredFields()).noneMatch(f -> f.isAnnotationPresent(Id.class))
                && Arrays.stream(fieldValue.getClass().getMethods()).noneMatch(m -> Objects.nonNull(m.getAnnotation(Id.class))))
        )
            throw new MappingException(ExceptionMessages.TRYING_SAVE_NO_DOCUMENT_OBJECT);
        return true;
    }
}
